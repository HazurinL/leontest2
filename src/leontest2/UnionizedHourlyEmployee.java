package leontest2;

public class UnionizedHourlyEmployee implements Employee{
	private double hourlyPay;
	private double hoursWeek;
	private double pensionContribution;

	public double getYearlyPay() {
		return((hoursWeek*52*hourlyPay)+pensionContribution);
	}
	public UnionizedHourlyEmployee(double hourly, double hoursweek, double pension) {
		this.hourlyPay = hourly;
		this.hoursWeek = hoursweek;
		this.pensionContribution = pension;
	}
}
