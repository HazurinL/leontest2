package leontest2;

public class PayrollManagement {
public double getTotalExpenses(Employee[] employees) {
	double moneyLost = 0;
	for (Employee x : employees) {
	moneyLost += x.getYearlyPay();
	}
	return moneyLost;
}
public static void main(String[] args) {
	Employee[] employees = new Employee[5];
	employees[0] = new SalariedEmployee(5000);
	employees[1] = new HourlyEmployee(30, 40);
	employees[2] = new UnionizedHourlyEmployee(40,30,5000);
	employees[3] = new SalariedEmployee(100000);
	employees[4] = new HourlyEmployee(13, 50);
	PayrollManagement payroll = new PayrollManagement();
	double allPays = payroll.getTotalExpenses(employees);
	
	System.out.println(allPays);
}
}
